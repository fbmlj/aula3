Rails.application.routes.draw do
  resources :habits
  resources :territories do
    resources :animals
  end
  get '/',to: 'static_page#welcome'
  resources :forms
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
