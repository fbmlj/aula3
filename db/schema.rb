# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_19_173156) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "animal_habits", force: :cascade do |t|
    t.bigint "animal_id"
    t.bigint "habit_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["animal_id"], name: "index_animal_habits_on_animal_id"
    t.index ["habit_id"], name: "index_animal_habits_on_habit_id"
  end

  create_table "animals", force: :cascade do |t|
    t.string "specie"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "territory_id"
  end

  create_table "feed_chains", force: :cascade do |t|
    t.integer "prey_id"
    t.integer "predator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forms", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "issue"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpf"
    t.date "birthdate"
    t.integer "gender", default: 1
  end

  create_table "habits", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "territories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "animal_habits", "animals"
  add_foreign_key "animal_habits", "habits"
end
