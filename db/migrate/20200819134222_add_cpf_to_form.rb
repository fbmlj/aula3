class AddCpfToForm < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :cpf, :string
  end
end
