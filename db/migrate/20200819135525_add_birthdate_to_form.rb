class AddBirthdateToForm < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :birthdate, :date
  end
end
