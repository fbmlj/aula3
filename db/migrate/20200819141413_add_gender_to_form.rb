class AddGenderToForm < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :gender, :integer,default: 1
  end
end
