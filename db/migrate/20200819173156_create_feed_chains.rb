class CreateFeedChains < ActiveRecord::Migration[5.2]
  def change
    create_table :feed_chains do |t|
      t.integer :prey_id
      t.integer :predator_id

      t.timestamps
    end
  end
end
