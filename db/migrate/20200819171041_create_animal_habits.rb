class CreateAnimalHabits < ActiveRecord::Migration[5.2]
  def change
    create_table :animal_habits do |t|
      t.references :animal, foreign_key: true
      t.references :habit, foreign_key: true

      t.timestamps
    end
  end
end
