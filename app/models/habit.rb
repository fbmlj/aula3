class Habit < ApplicationRecord
    has_many :animal_habits
    has_many :animals, through: :animal_habits
end


