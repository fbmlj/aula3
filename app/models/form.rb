class Form < ApplicationRecord
    validates :birthdate,:email,:cpf, :name, :issue, :message, presence: true
    validates :email, :cpf,uniqueness: true
    validates :cpf, length: {is: 11}
    validate :must_be_over_18

    def age
        ((Date.today - birthdate)/365).to_i 
    end

    enum gender: [
        "f",
        "m"
    ]
    

    
    private
    def must_be_over_18
        errors.add(:age, "precisar ser maior que 18")  if age < 18
    end

    def correct_cpf
        
        self.cpf = self.cpf.to_i * 2
    end

end
