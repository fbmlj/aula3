class Animal < ApplicationRecord
    belongs_to :territory
    has_many :animal_habits
    has_many :habits, through: :animal_habits

    has_many :preys_feed, foreign_key: :predator_id, class_name: 'FeedChain'
    has_many :predator_feed, foreign_key: :prey_id, class_name: 'FeedChain'

    has_many :preys, through: :preys_feed
    has_many :predators, through: :predator_feed

end

