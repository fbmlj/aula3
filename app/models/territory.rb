class Territory < ApplicationRecord
    has_many :animals, dependent: :destroy

end
